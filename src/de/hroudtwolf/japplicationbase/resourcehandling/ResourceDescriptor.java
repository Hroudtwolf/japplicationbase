/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling;

/**
 * @author 	Hroudtwolf
 * @brief	This is the interface of all resource descriptors.
 */
public interface ResourceDescriptor {

	/**
	 * Returns the object of the resource.
	 * 
	 * @return Object	The resource object of a specific type.
	 */
	public Object getResource();
	
	/**
	 * Return TRUE if the resource was successfuly loaded.
	 * 
	 * @return boolean	TRUE on success.
	 */
	public boolean isOk();
}
