/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling;


/**
 * @author 	Hroudtwolf
 * @brief 	This is the interface of the resource manager listeners.
 */
public interface ResourceManagerListener {
	
	/**
	 * Will be called when the resource manager wants to load the
	 * resources.
	 * 
	 * @param ResourceManager resourceManager		The resource manager object.
	 */
	public void onLoadingResources(ResourceManager resourceManager);
	
	/**
	 * Will be called when the resource manager wants to release the loaded
	 * resources.
	 * 
	 * @param ResourceManager resourceManager		The resource manager object.
	 */
	public void onReleasingResources(ResourceManager resourceManager);

	/**
	 * 
	 * 
	 * @param ResourceManager resourceManager		The resource manager object.
	 */
	public void onResourceDescriptorsSetup(ResourceManager resourceManager);
	
}
