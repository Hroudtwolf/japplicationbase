/**
 * 	@copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling;

/**
 * @author 	Hroudtwolf
 * @brief 	This is the exception class of the resource manager.
 */
public class ResourceManagerException extends Exception {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = -8721512903774745936L;

	/**
	 * Creates a resource manager exception.
	 * 
	 * @param String message		The exception message.
	 */
	public ResourceManagerException(String message) {
		super(message);
		
		return;
	}
}
