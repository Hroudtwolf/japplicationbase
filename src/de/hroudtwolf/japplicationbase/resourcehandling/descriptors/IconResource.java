/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 *  
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling.descriptors;

import java.net.URL;

import javax.swing.ImageIcon;

import de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor;

/**
 * @author 	Hroudtwolf
 * @brief	This is the descriptor of icon resources.
 */
public class IconResource implements ResourceDescriptor {

	private ImageIcon icon;
	
	/**
	 * Creates a descriptor for an image icon file resource.
	 * 
	 * @param URL url		The URL of the icon file.
	 */
	public IconResource(URL url) {
		
		try {
			this.icon = new ImageIcon(url);
		} catch (Exception e) {
			this.icon = null;
		}
		
		return;
	}
	
	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#getResource()
	 */
	@Override
	public ImageIcon getResource() {
		
		return this.icon;
	}

	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#isOk()
	 */
	@Override
	public boolean isOk() {
 
		return this.icon != null;
	}

}
