/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling.descriptors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor;

/**
 * @author 	Hroudtwolf
 * @brief	This is the descriptor of text file resources.
 */
public class TextResource implements ResourceDescriptor {

	private String text;
	
	/**
	 * Creates a descriptor for a text file resource.
	 * 
	 * @param URL url	The URL of the text file.
	 */
	public TextResource(URL url) {

		StringBuilder stringBuilder = new StringBuilder();
		
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(
			new InputStreamReader(url.openStream()));
		} catch (IOException e1) {
			this.text = null;
			return;
		}

		String inputLine;
        	try {
			while ((inputLine = bufferedReader.readLine()) != null) {
			    System.out.println(inputLine);
			}
			
			 bufferedReader.close();
		} catch (IOException e) {
			this.text = null;
			return;
		}
       
        	this.text = stringBuilder.toString();
        
		return;
	}
	
	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#getResource()
	 */
	@Override
	public String getResource() {
		
		return this.text;
	}

	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#isOk()
	 */
	@Override
	public boolean isOk() {
 
		return this.text != null;
	}

}
