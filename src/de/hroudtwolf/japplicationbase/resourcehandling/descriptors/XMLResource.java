/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling.descriptors;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor;

/**
 * @author	Hroudtwolf
 * @brief	This is the descriptor of XML file resources.
 */
public class XMLResource implements ResourceDescriptor {

	private Document document;

	/**
	 * Creates a descriptor for a XML file resource.
	 * 
	 * @param URL url	The URL of the resource.
	 */
	public XMLResource(URL url) {
 
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			this.document = null;
			return;
		}
		try {
			this.document = dBuilder.parse(url.openStream());
		} catch (Exception e) {
			this.document = null;
		}

		return;
	}

	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#getResource()
	 */
	@Override
	public Document getResource() {
		
		return this.document;
	}

	/* (non-Javadoc)
	 * @see de.hroudtwolf.japplicationbase.resourcehandling.ResourceDescriptor#isOk()
	 */
	@Override
	public boolean isOk() {
 
		return this.document != null;
	}
}
