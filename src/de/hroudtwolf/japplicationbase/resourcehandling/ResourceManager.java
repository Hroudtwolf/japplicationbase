/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.resourcehandling;

import java.util.HashMap;

/**
 * @author 	Hroudtwolf
 * @brief 	This is the resource manager.
 */
public class ResourceManager {

	private HashMap<String, ResourceDescriptor> resources = new HashMap<String, ResourceDescriptor>();
	private ResourceManagerListener resourceManagerListener;
	
	/**
	 * @param ResourceManagerListener resourceManagerListeners	The resource manager listener object.
	 * 																Is optional and can be NULL.
	 */
	public ResourceManager(ResourceManagerListener resourceManagerListener) {
		
		this.resourceManagerListener = resourceManagerListener;
		
		if (this.resourceManagerListener != null) {
			this.resourceManagerListener.onResourceDescriptorsSetup(this);
			this.resourceManagerListener.onLoadingResources(this);
		}
		
		return;
	}
	
	/**
	 * Releases all loaded resources.
	 */
	public void finish() {
		
		if (this.resourceManagerListener != null) {
			this.resourceManagerListener.onReleasingResources(this);
		}
		
		this.resources 				 = null;
		this.resourceManagerListener = null;
		
		return;
	}
	
	/**
	 * Adds a resource to the resource manager.
	 * 
	 * @param String name					The name of the resource.
	 * @param ResourceDescriptor resourceDescriptor		The descriptor of the resource.
	 */
	public void addResource(String name, ResourceDescriptor resourceDescriptor) {
		
		this.resources.put(name, resourceDescriptor);
		
		return;
	}
	
	/**
	 * @param   String name			The name of the resource.
	 * @return  ResourceDescriptor		The descriptor object of the resource.
	 * @throws  ResourceManagerException	if not resource with the specific name exists.
	 */
	public ResourceDescriptor getResource(String name) throws ResourceManagerException {
		
		if (! this.resources.containsKey(name)) {
			throw new ResourceManagerException(
				"Can't find '" + name + "' resource."
			);
		}
		
		return this.resources.get(name);
	}
}
