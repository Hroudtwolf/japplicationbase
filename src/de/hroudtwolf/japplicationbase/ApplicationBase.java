/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase;

import de.hroudtwolf.japplicationbase.configuration.ConfigHandler;
import de.hroudtwolf.japplicationbase.configuration.ConfigHandlerListener;
import de.hroudtwolf.japplicationbase.resourcehandling.ResourceManager;
import de.hroudtwolf.japplicationbase.resourcehandling.ResourceManagerListener;

/**
 * @author 		Hroudtwolf
 * @brief 		This class is the base class of the framework.
 */
public abstract class ApplicationBase {
	
	/**
	 * Keeps the resource manager object.
	 * 
	 * @var ResourceManager resourceManager
	 */
	private ResourceManager resourceManager;
	private ConfigHandler configHandler;
	
	/**
	 * The super constructor that initializes the 
	 * ApplicationBase super class.
	 * 
	 * @param ResourceManagerListener resourceManagerListener The resource manager listener object.
	 */
	public void initialize(ResourceManagerListener resourceManagerListener, 
			       ConfigHandlerListener configHandlerListener) {
		
		this.resourceManager 	= new ResourceManager(resourceManagerListener);
		this.configHandler	= new ConfigHandler(configHandlerListener);
			
		return;
	}
	
	/**
	 * Returns the resource manager object.
	 * 
	 * @return ResourceManager		The resource manager object
	 */
	public ResourceManager getResourceManager() {
		
		return this.resourceManager;
	}
	
	/**
	 * Returns the configuration handler object.
	 * 
	 * @return ConfigHandler	The configuration handler object.
	 */
	public ConfigHandler getConfigHandler() {
		
		return this.configHandler;
	}
}
