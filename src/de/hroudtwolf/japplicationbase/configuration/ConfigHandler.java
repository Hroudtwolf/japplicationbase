/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.configuration;

import java.util.Properties;

/**
 * @author 	Hroudtwolf
 * @brief	This is the configuration handler.
 */
public class ConfigHandler {

	private ConfigHandlerListener configHandlerListener;
	private Properties properties;
	private boolean setupDefaultsInProgress;

	/**
	 * Creates a configuration handler object.
	 * 
	 * @param ConfigHandlerListener listener	The listener object of the configuration handler.
	 * 						Is optional and can be NULL. 
	 * 											
	 */
	public ConfigHandler(ConfigHandlerListener listener) {
		
		this.configHandlerListener = listener;
		this.properties		   = new Properties();
		
		if (this.configHandlerListener != null) {
			this.setupDefaultsInProgress = true;
			this.configHandlerListener.onSettingDefaultConfiguration(this);
			this.configHandlerListener.onLoadingConfigurations(this);
			this.setupDefaultsInProgress = false;
		}
		
		return;
	}
	
	/**
	 * Retrieves a configuration value of a specific key.
	 * 
	 * @param  String key			The key of the configuration value.
	 * @return String			The configuration value on success.
	 */
	public String getConfig(String key) {
		
		if (this.properties.contains(key)) {
		    return null;
		}
		
		return this.properties.getProperty(key);
	}
	
	/**
	 * Sets or adds a configuration value for a specific key.
	 * 
	 * @param  String key			The key of the configuration value.
	 * @param  String			The configuration value on success.
	 */
	public void setConfig(String key, String value) {
		
		if (! this.setupDefaultsInProgress && this.configHandlerListener != null) {
			this.configHandlerListener.onConfigurationChanged(this, key);
		}
		
		this.properties.setProperty(key, value);
		
		return;
	}
}
