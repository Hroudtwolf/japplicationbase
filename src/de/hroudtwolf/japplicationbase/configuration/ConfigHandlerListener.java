/**
 *  @copyright	2013+ © By Marc Sven Kleinböhl (aka Hroudtwolf)
 * 
 *  This file is part of JApplicationBase.
 *
 *  JApplicationBase is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  JApplicationBase is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with JApplicationBase.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.hroudtwolf.japplicationbase.configuration;


/**
 * @author 	Hroudtwolf
 * @brief	This is the interface of the configuration handler listener.
 */
public interface ConfigHandlerListener {
	
	/**
	 * 
	 * 
	 * @param ConfigHandler configHandler		The configuration handler object.
	 */
	public void onSettingDefaultConfiguration(ConfigHandler configHandler);

	/**
	 * @param ConfigHandler configHandler		The configuration handler object.
	 * @param String 	key			The key of the value which has been changed.
	 */
	public void onConfigurationChanged(ConfigHandler configHandler, String key);
	
	/**
	 * 
	 * 
	 * @param ConfigHandler configHandler		The configuration handler object.
	 */
	public void onLoadingConfigurations(ConfigHandler configHandler);	
}
